/// <reference types="svelte" />
interface DirectoryElement {
    name: string;
    is_dir: boolean;
}
