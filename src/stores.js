import { writable, derived, readable } from "svelte/store";
import { platform } from "@tauri-apps/api/os";

// Writable stores
export const openDirectory = writable([]);
export const currentlyOpenFileName = writable("");
export const currentlyOpenFileContents = writable("");
export const showAbout = writable(false);


const osNameMap = {
    dragonfly: "Linux",
    netbsd: "Linux",
    openbsd: "Linux",
    linux: "Linux",
    freebsd: "Linux",
    win32: "Windows",
    darwin: "MacOS",
};

const languageMap = {
    rs: "Rust",
    cs: "C#",
    svelte: "Svelte",
    js: "JavaScript",
    ts: "TypeScript",
    c: "C/C++",
    h: "C/C++",
    cpp: "C/C++",
    hpp: "C/C++",
    vb: "VisualBasic",
    aspx: "ASP.NET",
    css: "CSS",
    html: "HTML",
}

// Readable stores
export const operatingSystem = readable("Unknown", async function start(set) {
    set(osNameMap[await platform()]);

    return function stop() {
        "";
    }
});

// Derived stores
export const currentLanguage = derived(currentlyOpenFileName, $currentlyOpenFileName => languageMap[$currentlyOpenFileName.split(".").reverse()[0]]);
