import {readDir, FileEntry} from "@tauri-apps/api/fs";
import {currentlyOpenFileName, currentlyOpenFileContents, openDirectory} from "./stores.js";

export async function readDirectory(directory: string, shouldSort: boolean): Promise<FileEntry[]> {
    console.log(directory);
    let dir = await readDir(directory, {recursive: true});
    return new Promise((resolve, reject) => {
        if (dir) {
            if (shouldSort) {
                dir = dir.sort((a, b) => a.children !== undefined && b.children !== undefined ? 0 : a.children !== undefined ? -1 : 1);
            }
            resolve(dir);
        } else {
            reject(`Failed to read directory ${directory}`);
        }
    });
}

export function closeEditor() {
    currentlyOpenFileName.set("");
    currentlyOpenFileContents.set("");
}

export function closeFolder() {
    openDirectory.set([]);
    closeEditor();
}
