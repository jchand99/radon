module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{html,js,ts,svelte}'],
  theme: {
    extend: {},
    fontFamily: {
      'sans': ['ui-sans-serif', 'system-ui'],
      'serif': ['ui-serif', 'Georgia'],
      'mono': ['ui-monospace', 'SFMono-Regular'],
      'code': ['Cascadia Code', 'SFMono-Regular'],
    },
  },
  plugins: [],
}
