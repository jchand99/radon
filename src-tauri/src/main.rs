#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use tauri::{CustomMenuItem, Menu, MenuItem, Submenu};

fn create_mac_menu() -> Menu {
    let new_file = CustomMenuItem::new("new-file", "New File");
    let open_folder = CustomMenuItem::new("open-folder", "Open Folder");
    let close_editor = CustomMenuItem::new("close-editor", "Close Editor");
    let close_folder = CustomMenuItem::new("close-folder", "Close Folder");
    let file_menu = Menu::new()
        .add_item(new_file)
        .add_native_item(MenuItem::Separator)
        .add_item(open_folder)
        .add_native_item(MenuItem::Separator)
        .add_item(close_editor)
        .add_item(close_folder)
        .add_native_item(MenuItem::CloseWindow);
    let file_submenu = Submenu::new("File", file_menu);

    let edit_menu = Menu::new()
        .add_native_item(MenuItem::Copy)
        .add_native_item(MenuItem::Paste);
    let edit_submenu = Submenu::new("Edit", edit_menu);

    let about = CustomMenuItem::new("about", "About");
    let help_menu = Menu::new().add_item(about);
    let help_submenu = Submenu::new("Help", help_menu);

    Menu::new()
        .add_submenu(file_submenu)
        .add_submenu(edit_submenu)
        .add_submenu(help_submenu)
}

// TODO: Add events for MacOS titlebar options; Remove custom titlebar from MacOS; add window
// decorations back to mac

fn main() {
    if cfg!(target_os = "macos") {
        tauri::Builder::default()
            .menu(create_mac_menu())
            .on_menu_event(|event| match event.menu_item_id() {
                "close" => {
                    event.window().close().unwrap();
                }
                "about" => {
                    println!("About!");
                }
                _ => {}
            })
            .invoke_handler(tauri::generate_handler![])
            .run(tauri::generate_context!())
            .expect("error while running tauri application");
    } else {
        tauri::Builder::default()
            .invoke_handler(tauri::generate_handler![])
            .run(tauri::generate_context!())
            .expect("error while running tauri application");
    }
}
